package com.xyz.motoblack;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.util.Log;


public class MenuAdapter extends FragmentGridPagerAdapter  {

    private final String TAG = ((Object) this).getClass().getName();

    public final OneFragment oneFragment;

    public MenuAdapter(Context context, FragmentManager fm) {
        super(fm);
        oneFragment = new OneFragment();
    }

    @Override
    public int getColumnCount(int arg0) {
        return 1;
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public Fragment getFragment(int rowNum, int colNum) {
        Log.d(TAG, String.format("getFragment(%d, %d)", rowNum, colNum));

        if(colNum == 0) {
            return oneFragment;
        }

        return null;
    }
}
package com.xyz.motoblack;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.GridViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;

public class WearActivity extends Activity {

    private final String TAG = ((Object) this).getClass().getName();

    private GridViewPager gridViewPager;
    private MenuAdapter menuAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Create(Wear)");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wear_activity);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                menuAdapter = new MenuAdapter(WearActivity.this, getFragmentManager());
                gridViewPager = (GridViewPager) findViewById(R.id.pager);
                gridViewPager.setAdapter(menuAdapter);
            }
        });
    }

}

package com.xyz.motoblack;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class OneFragment extends Fragment {

    private final String TAG = ((Object) this).getClass().getName();

    private TextView oneTextView;
    private TextView twoTextView;
    private TextView threeTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.onetwothree, container, false);

        oneTextView = (TextView) contentView.findViewById(R.id.status);
        twoTextView = (TextView) contentView.findViewById(R.id.two);
        threeTextView = (TextView) contentView.findViewById(R.id.three);

        oneTextView.setText("1");
        twoTextView.setText("2");
        threeTextView.setText("3");

        return contentView;
    }
}

package com.xyz.motoblack;

import android.content.Context;
import android.support.wearable.view.GridViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ScrollableGridViewPager extends GridViewPager {

    private float xDistance, yDistance, lastX, lastY;

    public ScrollableGridViewPager(Context context) {
        super(context);
    }

    public ScrollableGridViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch(ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setDownPosition(ev);
                break;
            case MotionEvent.ACTION_MOVE:
                if(isVerticalScroll(ev)) {
                    return false;
                }
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch(ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setDownPosition(ev);
                break;
            case MotionEvent.ACTION_MOVE:
                if(isVerticalScroll(ev)) {
                    return false;
                }
        }
        return super.onTouchEvent(ev);
    }

    private void setDownPosition(MotionEvent ev){
        xDistance = yDistance = 0f;
        lastX = ev.getX();
        lastY = ev.getY();
    }

    private boolean isVerticalScroll(MotionEvent ev){
        final float curX = ev.getX();
        final float curY = ev.getY();
        xDistance += Math.abs(curX - lastX);
        yDistance += Math.abs(curY - lastY);
        lastX = curX;
        lastY = curY;
        if(xDistance < yDistance) {
            return true;
        }
        return false;
    }
}
